/*******************************************************************************
* Anystreaming firmware writier
* =============================================================================
* (C)2016, 3iware rage.kim@gmail.com
*
* Anystreaming NAND flash blocks
* ------------------------------
* 0x00000000-0x00040000[ 256K ] : nbootldr  => /dev/mtd0
* 0x00040000-0x00080000[ 256K ] : nenv      => /dev/mtd1
* 0x00400000-0x00480000[ 512K ] : nkernel   => /dev/mtd2
* 0x01000000-0x01400000[  5MB ] : nroot     => /dev/mtd3
* 0x02800000-0x03D00000[ 40MB ] : nextra    => /dev/mtd4
*
* Below is partition refernce used to update firmware.
* The partion needs to be updated depending on the design
* ***********NAND****************
* 0x00000000-0x00040000 : "nand-bootldr"=>/dev/mtd4
* 0x00040000-0x00080000 : "nand-env"=>/dev/mtd5
* 0x00080000-0x00480000 : "nand-kernel"=>/dev/mtd6
* 0x00480000-0x01480000 : "nand-rootfs"=>/dev/mtd7
* 0x01480000-0x07480000 : "nand-ubifs"=>/dev/mtd8
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "crcgen.h"

#define DEF_PRG_STR         "Anystreaming FW to NAND writer"
#define DEF_VER_STR         "0.9.2"
#define DEF_FW_TARGZ        "/var/fwupgrade.zip"
#define DEF_FW_OUT_PATH     "/var/fwupgrade"
#define DEF_RESULT_FN       "/var/fwupgrade_result"

#define DEF_DT_BOOTLDR      "mvd_first_image.dat"
#define DEF_FW_BOOTLDR      "nf_bootimage.bin"

#define DEF_DT_KERNEL       "mvd_second_image.dat"
#define DEF_FW_KERNEL       "zImage"

#define DEF_DT_ROOTFS       "mvd_third_image.dat"
#define DEF_FW_ROOTFS       "rootfs.sqfs"

#define DEF_DEV_BOOTLDR     "/dev/mtd0"
#define DEF_SZ_BOOTLDR      256 * 1024

#define DEF_DEV_KERNEL      "/dev/mtd2"
#define DEF_SZ_KERNEL       512 * 1024

#define DEF_DEV_ROOTFS      "/dev/mtd3"
#define DEF_SZ_ROOTFS       5 * 1024 * 1024

#define DEF_DEV_EXTRA       "/dev/mtd4"
#define DEF_SZ_EXTRA        40 * 1024 * 1024

#ifndef bool
    #define bool    int
#endif // bool

#ifndef true
    #define true    1
#endif // true

#ifndef false
    #define false   0
#endif // false

////////////////////////////////////////////////////////////////////////////////

unsigned int nfbootldr_sz = 0;
unsigned int kernel_sz = 0;
unsigned int rootfs_sz = 0;

char mvdfile1[256] = {0};
char mvdfile2[256] = {0};
char mvdfile3[256] = {0};

char nfbootldr_fn[256] = {0};
char kernel_fn[256] = {0};
char rootfs_fn[256] = {0};

bool nfbootldr_enabled = false;
bool kernel_enabled = false;
bool rootfs_enabled = false;

////////////////////////////////////////////////////////////////////////////////

int getfilesz( const char* fn )
{
    int fsz = 0;

    if ( access( fn, F_OK ) == 0 )
    {
        FILE* fp = fopen( fn, "rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0, SEEK_END );
            fsz = ftell( fp );
            fseek( fp, 0, SEEK_SET );
            fclose( fp );
        }
    }

    return fsz;
}

bool removefile( const char* fn )
{
    if ( access( fn, F_OK ) == 0 )
    {
        if ( unlink( fn ) == 0 )
        {
            return true;
        }
    }

    return false;
}

bool extractfile( const char* srcfn, const char* dstfn )
{
    FILE* fpsrc = NULL;
    int   fszsrc = 0;

    FILE* fpdst = NULL;
    int   fszdst = 0;

    char* srcbuff = NULL;
    char* dstbuff = NULL;

    bool  retb = false;

    fpsrc = fopen( srcfn , "rb" );
    if ( fpsrc != NULL )
    {
        fseek( fpsrc, 0, SEEK_END );
        fszsrc = ftell( fpsrc );
        fseek( fpsrc, 0, SEEK_SET );

        if ( fszsrc > 0 )
        {
            srcbuff = (char*)malloc( fszsrc );
            if ( srcbuff != NULL )
            {
                fread( srcbuff, 1, fszsrc, fpsrc );
                fclose( fpsrc );

                // test source.
                fszdst = get_extracted_size( srcbuff );

                if ( fszdst > 0 )
                {
                    dstbuff = NULL;

                    int reti = extract_buffer( srcbuff, &dstbuff, fszdst );
                    if ( reti == fszdst )
                    {
                        if ( access( dstfn, F_OK ) == 0 )
                        {
                            if ( removefile( dstfn ) == false )
                            {
                                printf( "(ERROR:%s not removed)", dstfn );
                                dstbuff = NULL;
                                free( srcbuff );

                                return false;
                            }
                        }

                        fpdst = fopen( dstfn, "wb" );
                        if ( fpdst != NULL )
                        {
                            fwrite( dstbuff, 1, fszdst, fpdst );
                            fclose( fpdst );

                            retb = true;
                        }

                        dstbuff = NULL;
                        free ( srcbuff );

                        return retb;
                    }
#ifdef DEBUG
                    else
                    {
                        printf( "(D.ERROR: CRC E %d)", reti );
                    }
#endif // DEBUG

                    dstbuff = NULL;
                }
                else
                {
                    printf( "(ERROR:%d)", fszdst );
                }

                free( srcbuff );
            }
        }
#ifdef DEBUG
        else
        {
            printf( "(D.ERROR: file size  = 0)" );
        }
#endif // DEBUG
    }

    return false;
}

int main(int argc, char* argv[])
{
	char* readfile = NULL;
	char  execute[200] = {0};
	int   offset = 0;

    printf( "%s, version %s, rage.kim@3iware.com\n", DEF_PRG_STR, DEF_VER_STR );
#ifdef DEBUG
    printf( " ** DEBUG BUILD ** \n" );
#endif // DEBUG
    printf( "\n" );

    removefile( DEF_RESULT_FN );

    // check fw source file exsits
    printf( " * Checking FW exsits ... " );
    if ( access( DEF_FW_TARGZ, F_OK ) != 0 )
    {
        printf("nope, no file found.\n");
        return -1;
    }
    else
    {
        printf("found.\n");
    }

    /// -------------------------------------------------------------------------
    printf( " * Preparing destination dir ... ");
    if ( access( DEF_FW_OUT_PATH, F_OK ) != 0 )
    {
        sprintf( execute, "mkdir %s", DEF_FW_OUT_PATH );
        system( execute );
    }
    else
    {
        sprintf( execute, "rm -rf %s/*", DEF_FW_OUT_PATH );
        system( execute );
    }

    if ( access( DEF_FW_OUT_PATH, F_OK ) != 0 )
    {
        printf( "Failure.\n" );
        return -2;
    }

    printf( "Ok.\n" );
    printf( " * Extracting files ... " );

    sprintf( execute, "unzip -o %s -d %s", DEF_FW_TARGZ, DEF_FW_OUT_PATH );
    system( execute );
    printf( "Done.\n" );

    printf( " * Checking extracted files ...\n" );

    sprintf( mvdfile1,      "%s/%s", DEF_FW_OUT_PATH, DEF_DT_BOOTLDR );
    sprintf( nfbootldr_fn,  "%s/%s", DEF_FW_OUT_PATH, DEF_FW_BOOTLDR );

    sprintf( mvdfile2,  "%s/%s", DEF_FW_OUT_PATH, DEF_DT_KERNEL );
    sprintf( kernel_fn, "%s/%s", DEF_FW_OUT_PATH, DEF_FW_KERNEL );

    sprintf( mvdfile3,  "%s/%s", DEF_FW_OUT_PATH, DEF_DT_ROOTFS );
    sprintf( rootfs_fn, "%s/%s", DEF_FW_OUT_PATH, DEF_FW_ROOTFS );

    printf("\t->NAND flash boot loader : " );
    nfbootldr_sz = getfilesz( mvdfile1 );
    if ( nfbootldr_sz > 0 )
    {
        if ( extractfile( mvdfile1, nfbootldr_fn ) == true )
        {
            nfbootldr_sz = getfilesz( nfbootldr_fn );
            if ( nfbootldr_sz > 0 )
            {
                nfbootldr_enabled = true;
                printf( "Found: %d bytes required.\n", nfbootldr_sz );
            }
            else
            {
                printf( "Failed to extract file = %d\n", nfbootldr_sz );
            }
        }
        else
        {
            printf( "Failed to extract file !\n" );
        }
    }
    else
    {
        printf( "Not found.\n" );
    }

    printf("\t->kernel image : " );
    kernel_sz = getfilesz( mvdfile2 );
    if ( kernel_sz > 0 )
    {
        if ( extractfile( mvdfile2, kernel_fn ) == true )
        {
            kernel_sz = getfilesz( kernel_fn );
            if ( kernel_sz > 0 )
            {
                kernel_enabled = true;
                printf( "Found: %d bytes required.\n", kernel_sz );
            }
            else
            {
                printf( "Failed to extract file = %d\n", kernel_sz );
            }
        }
        else
        {
            printf( "Failed to extract file !\n" );
        }
    }
    else
    {
        printf( "Not found.\n" );
    }

    printf("\t->root squash file system image : " );
    rootfs_sz = getfilesz( mvdfile3 );
    if ( rootfs_sz > 0 )
    {
        if ( extractfile( mvdfile3, rootfs_fn ) == true )
        {
            rootfs_sz = getfilesz( rootfs_fn );
            if ( rootfs_sz > 0 )
            {
                rootfs_enabled = true;
                printf( "Found : %d bytes required.\n", rootfs_sz );
            }
            else
            {
                printf( "Failed to extract file : %d\n", rootfs_sz );
            }
        }
        else
        {
            printf( "Failed to extract file !\n" );
        }
    }
    else
    {
        printf( "Not found.\n" );
    }

    printf( "\n" );
    system( "sync" );

    /// -------------------------------------------------------------------------

    printf( " *******************************************************************\n" );
    printf( " * ! NAND FLASHING WARNNING !                                      *\n" );
    printf( " * --------------------------------------------------------------- *\n" );
    printf( " *  Do not turn off or quit program until image flashing done !    *\n" );
    printf( " *******************************************************************\n" );
    printf( "\n" );

    if ( nfbootldr_enabled == true )
    {
        printf( " * Erasing NAND flash boot loader area ... " );
        sprintf( execute,
                 "mtd_debug erase %s 0 0x%x",
                 DEF_DEV_BOOTLDR,
                 DEF_SZ_BOOTLDR );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Writing NAND flash boot loader (%d bytes) ... ", nfbootldr_sz );
        sprintf( execute,
                 "mtd_debug write %s 0 0x%x %s",
                 DEF_DEV_BOOTLDR,
                 //nfbootldr_sz,
                 DEF_SZ_BOOTLDR,
                 nfbootldr_fn );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Removing NAND flash boot loader file ... " );
        sprintf( execute, "rm -rf %s", nfbootldr_fn );
        system( execute );
    }
    else
    {
        printf( " * Skipping NAND flash boot loader.\n" );
    }

    if ( kernel_enabled == true )
    {
        printf( " * Erasing kerenl area ... " );
        sprintf( execute,
                 "mtd_debug erase %s 0 0x%0x",
                 DEF_DEV_KERNEL,
                 DEF_SZ_KERNEL );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Writing kernel image (%d bytes) ... ", kernel_sz );
        sprintf( execute,
                 "mtd_debug write %s 0 0x%x %s",
                 DEF_DEV_KERNEL,
                 //kernel_sz,
                 DEF_SZ_KERNEL,
                 kernel_fn );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Removing kernel image file ... " );
        sprintf( execute, "rm -rf %s", kernel_fn );
        system( execute );
    }
    else
    {
        printf( " * Skipping kernel image.\n" );
    }

    if ( rootfs_enabled == true )
    {
        printf( " * Erasing root squash file system area ... " );
        sprintf( execute,
                 "mtd_debug erase %s 0 0x%0x",
                 DEF_DEV_ROOTFS,
                 DEF_SZ_ROOTFS );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Writing root squash file system (%d bytes) ... ", rootfs_sz );
        sprintf( execute,
                 "mtd_debug write %s 0 0x%x %s",
                 DEF_DEV_ROOTFS,
                 //rootfs_sz,
                 DEF_SZ_ROOTFS,
                 rootfs_fn );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Erasing extra area ... " );
        sprintf( execute,
                 "mtd_debug erase %s 0 0x%0x",
                 DEF_DEV_EXTRA,
                 DEF_SZ_EXTRA );
        system( execute );
        printf( "Ok.\n" );

        sleep( 1 );

        printf( " * Removing root squash file system file ... " );
        sprintf( execute, "rm -rf %s", rootfs_fn );
        system( execute );
    }
    else
    {
        printf( " * Skipping root squash file system file.\n" );
    }

    printf( "\n" );
    printf( " * Done.\n" );

    sprintf( execute,
             "echo COMPLETE > %s",
             DEF_RESULT_FN );

    system( "sync" );
    sleep( 1 );

	return 0;
}
