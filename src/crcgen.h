#ifndef __CRCGEN_H__
#define __CRCGEN_H__

int get_extracted_size( const char* buff );
int extract_buffer( const char* srcbuff, const char** dstbuff, int dstbuffsz );

#endif // __CRCGEN_H__
